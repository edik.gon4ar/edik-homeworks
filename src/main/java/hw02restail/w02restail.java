package hw02restail;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;


public class w02restail {


        public static void main(String[] args) {
            final int ROWS = 5;
            final int COLUMNS = 5;
            char[][] grid = new char[ROWS][COLUMNS];
            int targetRow = new Random().nextInt(ROWS);
            int targetColumn = new Random().nextInt(COLUMNS);
            boolean gameover = false;

            // заполнение массива "-" и отметка цели
            for (int i = 0; i < ROWS; i++) {
                for (int j = 0; j < COLUMNS; j++) {
                    if (i == targetRow && j == targetColumn) {
                        grid[i][j] = 'x'; // отмечаем цель
                    } else {
                        grid[i][j] = '-'; // заполняем "-"
                    }
                }
            }

            // вывод начального состояния поля
            System.out.println("All set. Get ready to rumble!\n");
            printGrid(grid);

            // игровой цикл
            while (!gameover) {
                Scanner scanner = new Scanner(System.in);

                // получение координат выстрела
                int row, column;
                do {
                    System.out.print("Enter a row (1-5): ");
                    row = scanner.nextInt() - 1;
                } while (row < 0 || row >= ROWS);

                do {
                    System.out.print("Enter a column (1-5): ");
                    column = scanner.nextInt() - 1;
                } while (column < 0 || column >= COLUMNS);

                // обработка выстрела
                if (grid[row][column] == 'x') {
                    grid[row][column] = 'x'; // отмечаем пораженную цель
                    gameover = true;
                    System.out.println("You have won!");
                } else if (grid[row][column] == '-') {
                    grid[row][column] = '*'; // отмечаем место выстрела
                    System.out.println("Miss!");
                } else {
                    System.out.println("You've already shot there!");
                }

                // вывод обновленного поля
                printGrid(grid);
            }
        }

        // метод для вывода поля в консоль
        private static void printGrid(char[][] grid) {
            System.out.println("  | 1 | 2 | 3 | 4 | 5 |");
            System.out.println("-----------------------");
            for (int i = 0; i < grid.length; i++) {
                System.out.print(i + 1 + " | ");
                for (int j = 0; j < grid[i].length; j++) {
                    System.out.print(grid[i][j] + " | ");
                }
                System.out.println();
                System.out.println("-----------------------");
            }
        }
    }
