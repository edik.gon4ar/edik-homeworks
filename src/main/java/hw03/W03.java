package hw03;

import java.util.Scanner;

public class W03 {
    public static void main(String[] args) {
        Scanner Scanner = new Scanner(System.in);
        String[][] scedule = new String[7][2];
        scedule[0][0] = "Sunday";
        scedule[0][1] = "do home work";
        scedule[1][0] = "Monday";
        scedule[1][1] = "go to courses; watch a film";

        scedule[2][0] = "Tuesday";
        scedule[2][1] = "do class work";
        scedule[3][0] = "Wednesday";
        scedule[3][1] = "go to sleep";
        scedule[4][0] = "Thursday";
        scedule[4][1] = "do drink";
        scedule[5][0] = "Friday";
        scedule[5][1] = "go to long sleep; watch a film";
        scedule[6][0] = "Saturday";
        scedule[6][1] = "go to work; swimming";

        for (int i = 1; i < 100; i++) {
            System.out.print("\nPlease, input the day of the week: ");
            String Input = Scanner.nextLine();
            Input = Input.replaceAll("\\s", "");
            String Input0 = Input.toLowerCase();
            String Day = Input0.substring(0, 1).toUpperCase() + Input0.substring(1);

            if (Input0.equals("exit")) {
                System.exit(0);
            }
            switch (Day) {
                case "Sunday" -> System.out.print("Your tasks for Sunday: " + scedule[0][1]);
                case "Monday" -> System.out.print("Your tasks for Monday: " + scedule[1][1]);
                case "Tuesday" -> System.out.print("Your tasks for Tuesday: " + scedule[2][1]);
                case "Wednesday" -> System.out.print("Your tasks for Wednesday: " + scedule[3][1]);
                case "Thursday" -> System.out.print("Your tasks for Thursday: " + scedule[4][1]);
                case "Friday" -> System.out.print("Your tasks for Friday: " + scedule[5][1]);
                case "Saturday" -> System.out.print("Your tasks for Saturday: " + scedule[6][1]);
                default -> System.out.print("Sorry, I don't understand you, please try again.\n");
            }

        }
    }
}
